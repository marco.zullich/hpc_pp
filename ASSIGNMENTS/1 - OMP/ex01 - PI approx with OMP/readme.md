# Pi approximation with OpenMPI

Approximate pi.

File `ex01_serial.c` contains the serial implementation, while `ex01_parallel.c` the parallel one with the `#pragma omp atomic` method for the reduction of pi; file `ex01_parallel_critical.c` uses `#pragma omp critical`, while `ex01_parallel_reduce.c` uses `#pragma omp reduce`.

Compile with `icc filename.c -qopenmp` (Intel compiler).

Submit script with `qsub -d "$PWD" -l nodes=1:ppn=20 scalability.sh`. The script will compile the three scripts and run them on 1, 2, 4, 8, 16 and 20 threads 50 times per each number of threads, and then will print the resulting time (for each run) on a new folder called `results`. There will be three separate files, one for each program.

Scalability plot with granularity 1e7:
![scalability](scalability.jpeg "scalability plot")

Time was averaged over 50 runs per thread number.

We see how all three methods yield similar results, with 8 being the optimal number of threads.
