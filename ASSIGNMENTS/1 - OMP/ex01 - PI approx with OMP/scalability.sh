nthreads=(1 2 4 8 16 20)
reps=50

module purge
module load openmpi
module load intel

echo "Loaded modules"

#create folder and files to store results
rm -r results
mkdir results
cd results
touch atomic_results.txt
touch critical_results.txt
touch reduce_results.txt
cd ..

echo "Created the file for storing results"

#compile the 3 scripts
icc ex01_parallel.c -o atomic.out -qopenmp
icc ex01_parallel_critical.c -o critical.out -qopenmp
icc ex01_parallel_reduce.c -o reduce.out -qopenmp
echo "All files compiled. Now running"

#loop each file 50 times for each of the specified number of threads
for nthread in ${nthreads[@]}; do
    echo "Number of threads ${nthread}"
    for i in $(seq 1 $reps); do

        #keep only lines of the output containing either 'Num threads' or 'time'
        #and keep only the numerical data
        OMP_NUM_THREADS=${nthread} ./atomic.out | egrep "Num threads|time" | sed -r ':a;N;$!ba;s/Num threads: ([0-9]+)\ntime:/\1/g' >>results/atomic_results.txt
        OMP_NUM_THREADS=${nthread} ./critical.out | egrep "Num threads|time" | sed -r ':a;N;$!ba;s/Num threads: ([0-9]+)\ntime:/\1/g' >>results/critical_results.txt
        OMP_NUM_THREADS=${nthread} ./reduce.out | egrep "Num threads|time" | sed -r ':a;N;$!ba;s/Num threads: ([0-9]+)\ntime:/\1/g' >>results/reduce_results.txt
    done
done

echo "Finished running"

#clean everything
rm ./atomic.out ./critical.out ./reduce.out

module purge

echo "Done"