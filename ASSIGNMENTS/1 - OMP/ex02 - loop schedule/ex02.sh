#define variables for easy compilation of files
schedule_type=(static dyn)
chunk_size=(0 1 10)

#load modules and make space for results
module purge
module load openmpi
module load intel

rm -rf results
mkdir results

#compile and run
for schtype in ${schedule_type[@]}; do
    for chsize in ${chunk_size[@]}; do
        icc ex02_parallel_${schtype}_${chsize}.c -qopenmp

        OMP_NUM_THREADS=10 ./a.out | (echo ''; echo "${schtype} allocation - chunk size ${chsize}" && cat) >> results/schedule_results.txt
    done
done

rm ./a.out