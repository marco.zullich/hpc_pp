#include <stdlib.h>
#include <stdio.h>
#include <omp.h>


//predefined function
void print_usage( int * a, int N, int nthreads ) {

	int tid, i;
	for( tid = 0; tid < nthreads; ++tid ) {

		fprintf( stdout, "%d: ", tid );

		for( i = 0; i < N; ++i ) {

			if( a[ i ] == tid) fprintf( stdout, "*" );
			else fprintf( stdout, " ");
		}
		printf("\n");
	}
}



int main( int argc, char * argv[] ) {
	//length of array
	
	const int N = 100;
	int a[N];
	int thread_id = 0;
	
	#pragma omp parallel private (thread_id)
	{
		int n_threads = omp_get_num_threads();
		int thread_id = omp_get_thread_num();
		
		int i;
		#pragma omp for schedule (static)
		for(i = 0; i < N; ++i) { 
			//assign number of thread to each array position which is responsible
			//of that portion of the array
			a[i] = thread_id;
		}

		#pragma omp single nowait
		{
			//print which part of the array is currently being used by which thread
			//single clause -> only one thread will do the printing
			//nowait clause -> avoids placing a barrier here since the execution of this
			//slice of code doesn't modify the content of the array a
			print_usage(a,N,n_threads);
		}
	}	

	return 0;
}
