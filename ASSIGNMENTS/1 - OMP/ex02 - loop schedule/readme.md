# Lab 1 - Exercise 2

The following scrpits have been produced:

* `ex02_parallel_static_0.c` contains the parallel script with static schedule for loop, no chunk size specified;
* `ex02_parallel_static_1.c` contains the parallel script with static schedule for loop, chunk size 1;
* `ex02_parallel_static_10.c` contains the parallel script with static schedule for loop, chunk size 10;
* `ex02_parallel_dynamic_0.c` contains the parallel script with dynamic schedule for loop, no chunk size specified;
* `ex02_parallel_dynamic_1.c` contains the parallel script with dynamic schedule for loop, chunk size 1;
* `ex02_parallel_dynamic_10.c` contains the parallel script with dynamic schedule for loop, chunk size 10;

Note that the array size has been limited to 100 to improve readability of the stdout.

Compile with `icc filename.c -qopenmp`.

## Job submitting

The script `ex02.sh` will compile run the code on 10 threads for all of the 6 scripts above.

To submit it, type `qsub -d "$PWD" -l nodes=1:ppn=10 ex02.sh`. It will store the stdout printings in a subfolder (named `results`) of the current folder.

