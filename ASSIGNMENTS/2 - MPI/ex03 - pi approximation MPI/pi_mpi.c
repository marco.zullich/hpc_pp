#include<stdlib.h>
#include<stdio.h>
#include<mpi.h>
#include<assert.h>
#include "../cptimer.h"

int main( int argc, char * argv[] ) {
    int rank = 0;
    int npes = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &npes);

    


    //the two intervals extrema
    const double a = 0.0;
    const double b = 1.0;
    
    //granularity of partition of the interval
    const int N = 1e9;

    assert(N % npes == 0);

    //the height of the sub-interval, also the step size of the summation
    const double height = (b-a)/N;

    //chunk size -> size of the problem partaining to actual process
    int chunk_size = N/npes;

    //chunk_start -> where starts the portion of the partition
    //partaining to the current process?
    int chunk_start = rank * chunk_size;

    double start = seconds();
    int i;
    double partial_pi = 0.0;

    

    for(i = chunk_start ; i < chunk_start + chunk_size; i++){
        //midpoint of the interval
        double midpoint = a+i*height+height/2;
        //approximation of the function in the midpoint
        double f_approx = 1.0/(1.0 + midpoint*midpoint);
        
        //area of the interval
        double rec_area = height*f_approx;
        
        //sum to approx of pi of current process
        partial_pi += rec_area;
        
    }
    
    
    //variable to store the final value of pi
    double pi_approx;

    //operate reduction on pi_approx of all partial_pi
    MPI_Reduce(&partial_pi, &pi_approx, 1, MPI_DOUBLE, MPI_SUM, npes-1, MPI_COMM_WORLD);

    pi_approx *= 4;
    //barrier here is needed to measure time - I want to measure time when every process has ended
    MPI_Barrier(MPI_COMM_WORLD);
    double tot_time = seconds()-start;

    if(rank==npes-1)
        MPI_Send(&pi_approx, 1, MPI_DOUBLE, 0, 101, MPI_COMM_WORLD);
    if(rank==0)
        MPI_Recv(&pi_approx, 1, MPI_DOUBLE, npes-1, 101, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    

    if(rank==0){
        fprintf(stdout, "Approx of pi is %.6f\n", pi_approx);
        //process 0 now prints also the elapsed time
        fprintf(stdout, "Elapsed time: %.6f\n", tot_time);
    }

    MPI_Finalize();
    return 0;


}