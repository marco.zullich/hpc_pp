# Ring vector accumulation

## Blocking communications version:

File name: `ringsum.c`.

The program first allocates, then initializes two vectors, called `current` and `incoming`.
`current` holds the accumulation of all the vectors which have been sent to the process, while `incoming` stores the values of the last vector which has been sent by the previous process in the line.

The heart of the algorithm is a for loop where a synchronous send-receive is performed, sending the `incoming` vector to the next process, then receiving the vector sent by the previous process, and storing it within `incoming`; then `incoming` is summed to `current`.

Eventually, the program prints in the `stdout` the last array received by the process and the accumulation of the arrays received.

### Compilation and run

After having loaded the module `openmpi`, compile with `mpicc ringsum.c`, run with `mpirun -np <number of processes> ./a.out`.

## Non-blocking (overlapping) communications

File name: `ringsum_overlap.c`.

The corresponding C script is called `ringsum_overlap.c`. Compile and execute like explained in previous block.

The program gets slightly modified: there's a first intial communication which is serial, needed for the first accumulation to begin.

Then, a cycle begins in which the array which has just been received gets asynchronously passed along the chain via `Isend`; as soon as this happens, accumulation starts, and then data from the previous process in the chain gets received with `Recv`. This allows for data to be sent onward while summing the previously received array, ensuring that this received array does not get altered in the process.