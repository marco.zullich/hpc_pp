#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "../../cptimer.h"

#define N 10

void accumulate(int* a, int* b){
    int i;
    for(i=0; i<N; i++){
        a[i] += b[i];
    }
}

void printVec(int* v, int size){
    int i;
    for(i=0; i<N; ++i){
        fprintf(stdout, "%d ", v[i]);
    }
    fprintf(stdout, "\n");
}

void printSituation(int rank, int* current, int* incoming, int step){
    fprintf(stdout, "rank %d- step %d:\n", rank, step);
    fprintf(stdout, "   current\n\t");
    printVec(current, N);
    fprintf(stdout, "   incoming\n\t");
    printVec(incoming, N);
}

int main (int argc, char * argv[] ) {
    int rank, npes;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &npes);

    int* current, * incoming;
    int vecsize = N * sizeof(int);

    current = (int*)malloc(vecsize);
    incoming = (int*)malloc(vecsize);

    //Initialize current vector
    int i;
    for( i=0; i<N; i++){
        current[i] = rank*2 + i%3; //just a silly initialization formula
        incoming[i] = current[i]; //first send won't have an incoming vector -> copy current into incoming
    }

    double begin = seconds();

    for(i = 0; i<npes-1; i++){
        //send current thread to next process in line - % needed to let wraparound when npes is reached
        MPI_Send(incoming, N, MPI_INT, (rank + 1)%npes, 101, MPI_COMM_WORLD);
        //receive msg from previous thread
        MPI_Recv(incoming, N, MPI_INT, (rank - 1 + npes)%npes, 101, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        accumulate(current, incoming);
    }

    MPI_Barrier(MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    double tm = seconds()-begin;

    fprintf(stdout, "Elapsed: %f\n", tm);
    printSituation(rank, current, incoming, i+1);


    MPI_Finalize();
    return 0;

}