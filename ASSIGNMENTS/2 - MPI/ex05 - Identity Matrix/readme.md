# Identity matrix printing with OpenMPI

The algorithm creates an identity matrix and either prints it or saves it (depending on its size).

Since we're replicating the data across multiple processors, we want to allocate memory only on the portion relative to the process itself.

Each process hence gets an equal share of the matrix (with exceptions for the remainder). The matrix is split between processes row-wise. A process handles a number of rows equal to `size/n_rows` or `size/n_rows + 1` -> `rows_process`

The matrix inside the process is allocated as a single vector of size `N * rows_process`.
The allocation proceeds as follows: a conditional operator is used to identify the cases in which the vector's index corresponds to a case in which the row and column index in the whole matrix would correspond to the diagonal. A nested conditional operator accounts for the case in which the `size/n_rows` division has remainder to account for the imbalancement in the `rows_process` between processes.

The algorithms proceeds as follows:

* All processes with rank > 0 send their portion of the matrix to process 0;
* Process 0 starts sequentially gathering other processes' matrices in a separate buffer; while the gathering is carried out, it prints the current content of the matrix. When the printing is done, it waits until the buffer is received, it overwrites the content of the matrix with the just-received buffer.
Again, the `rows_processes` gets decreased when there's imbalancement in the `rows_processes` between processes and all matrices coming from processes with a larger `rows_processes` have been printed.

## Compilation and run

After having loaded module `openmpi`, compile with `mpicc squarematrix.c`, run with `mpirun -np <number of processes> ./a.out <size of matrix>`.