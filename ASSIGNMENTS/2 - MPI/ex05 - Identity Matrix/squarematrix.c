#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


void print_matrix_byrow(int* m, int row_size, int col_size, FILE* f){
    int i,j;
    for(i=0; i<row_size; i++){
        for(j=0; j<col_size; j++)
            fprintf(f, "%d ", m[i*col_size + j]);
        fprintf(f, "\n");
    }
}


int main( int argc, char * argv[] ) {
    
    const int N = atoi(argv[1]);

    int rank, npes;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &npes);

    MPI_Request request;

    int rows_process = N/npes;
    int rem = N % npes;

    

    if(rank<rem){
        rows_process++;
    }

    int i, j;

    int* mat = (int*) malloc (rows_process * N * sizeof(int));

    for(i=0; i<rows_process; ++i){
        for(j=0; j<N; j++){
            mat[i*N + j] =  (j == (rank * rows_process) + (rank>=rem & rem>0 ? rem : 0) + i) ? 1 : 0;
        }
    }
    
    
    FILE* f = stdout;
    if(N>=10)
        f = fopen("squarematrix.bin","wb");
    

    if(rank>0){
        MPI_Send(mat, rows_process * N, MPI_INT, 0, 101, MPI_COMM_WORLD);
    }else{
        int proc;
        
        for(proc = 1; proc < npes; proc++){
            int* recv_buf = (int *)malloc(rows_process * N * sizeof(int));
            //start receiving next process' submatrix in a separate buffer  
            MPI_Irecv(recv_buf, N * rows_process, MPI_INT, proc, 101, MPI_COMM_WORLD, &request);
            //print current chunk of matrix
            print_matrix_byrow(mat, rows_process, N, f);
            //wait for asynchronous receiving to end
            MPI_Wait(&request, MPI_STATUS_IGNORE);

            free(mat); //content of mat no more needed, might as well deallocate it
            mat = recv_buf; //mat pointer points now to receiving buffer

            //if remainder greater then 0, decrement process size when the process rank + 1
            //is the same as the remainder - this handles the case in which all processes don't have
            //the same number of rows per process
            if(proc == rem)
                rows_process--;
        }
        //final printing - print final buffer received but not printed yet
        print_matrix_byrow(mat, rows_process, N, f);

        fclose(f);
    }
        
    MPI_Finalize();
    return 0;

}