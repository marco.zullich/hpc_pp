#include<stdio.h>
#include<assert.h>
#include "../cptimer.h"

#define N 8192
#define MAX_SIZE_SMALLBLOCK_X 32
#define MAX_SIZE_SMALLBLOCK_Y 32

void random_ints(int *p, int n) {
	int i;
	for(i=0; i<n; i++) {
		p[i]=rand()%50;
	}
}

void sqmatprint(int *m){

    for(int i=0; i< 10; i++){
        for(int j=0; j< 10; j++){
            fprintf(stdout, "%d, ", m[i*N+j]);
        }
        fprintf(stdout,"...\n");
    }
    
    fprintf(stdout, "...\n");

    for(int i=N-11; i<N; i++){
        fprintf(stdout,"...");
        for(int j=N-11; j<N; j++){
            fprintf(stdout, "%d, ", m[i*N+j]);
        }
        fprintf(stdout,"\n");
    }
}

__global__ void transpose(int* matrix_in, int* matrix_out){ ///, int* intermediate_out){


    int col_idx = blockIdx.x * blockDim.x + threadIdx.x;
    int row_idx = blockIdx.y * blockDim.y + threadIdx.y;
    
    //allocate small block of memory in shared memory (size 1 MB)
    __shared__ int smallblock_in [MAX_SIZE_SMALLBLOCK_X][MAX_SIZE_SMALLBLOCK_Y]; //this holds the original matrix chunk

    //define correspondence between smallblock_in and original matrix
    smallblock_in[threadIdx.x][threadIdx.y] = matrix_in[row_idx*N + col_idx];

    __syncthreads();
    
    matrix_out[col_idx*N + row_idx] = smallblock_in[threadIdx.x][threadIdx.y];
}

bool transpositionCorrect(int* matrix_in, int* matrix_out){
    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            if(matrix_in[i*N+j] != matrix_out[j*N+i])
                return false;
    return true;
}

int main(int argc, const char * argv[] ){

    int threads_per_block = 256;

    if(argc>=2){
        threads_per_block = atoi(argv[1]);
        if(threads_per_block>1024){
            fprintf(stderr, "Max number of threads per block is 1024\n");
            exit(EXIT_FAILURE);
        }
        if(threads_per_block%32!=0){
            fprintf(stderr, "Threads per block need to be multiple of 32\n");
            exit(EXIT_FAILURE);
        }
    }


    fprintf(stdout, "Thread per block: %d\n", threads_per_block);

    int * m, * o; //HOST
    int * dev_m, * dev_o; //DEVICE

    int matsize = N * N * sizeof(int);


    //Allocate
    m = (int*)malloc(matsize);
    o = (int*)malloc(matsize);

    cudaMalloc((void**) &dev_m, matsize);
    cudaMalloc((void**) &dev_o, matsize);

    //Initialize
    random_ints(m, N*N);

    //Copy
    cudaMemcpy(dev_m, m, matsize, cudaMemcpyHostToDevice);

    //Define 2d grid for matrix subdivision

    dim3 block (MAX_SIZE_SMALLBLOCK_X, threads_per_block/MAX_SIZE_SMALLBLOCK_X);
    dim3 grid (N/block.x, N/block.y);

    //Run kernel
    double start = seconds();

    transpose<<< grid , block >>>(dev_m, dev_o);
    cudaDeviceSynchronize();
    double time_exec = seconds()-start;

    // copy device result back to host copy of c
    cudaMemcpy( o, dev_o, matsize,   cudaMemcpyDeviceToHost );

    
    fprintf(stdout, "Orig mx\n");
    sqmatprint(m);

    fprintf(stdout, "Transposed mx\n");
    sqmatprint(o);

    fprintf(stdout, "Execution time: %.6f\n", time_exec);

    fprintf(stdout, "Bandwidth: %.6f\n", matsize * 2 / time_exec / 1e9);

    bool correct = transpositionCorrect(m, o);
    fprintf(stdout, "Transposition is %s\n", correct ? "correct" : "wrong");

    free(m); free(o); cudaFree(dev_m); cudaFree(dev_o);

    return 0;



}