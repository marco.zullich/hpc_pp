#include <stdio.h>
#include "../cptimer.h"

#define MAX_THR_PER_BLOCK 1024
#define N 8192

//for init
void random_ints(int *p, int n) {
	int i;
	for(i=0; i<n; i++) {
		p[i]=rand()%50;
	}
}

//print large matrix
void sqmatprint(int *m){

    for(int i=0; i< 10; i++){
        for(int j=0; j< 10; j++){
            fprintf(stdout, "%d, ", m[i*N+j]);
        }
        fprintf(stdout,"...\n");
    }
    
    fprintf(stdout, "...\n");

    for(int i=N-11; i<N; i++){
        fprintf(stdout,"...");
        for(int j=N-11; j<N; j++){
            fprintf(stdout, "%d, ", m[i*N+j]);
        }
        fprintf(stdout,"\n");
    }
}

//check if transposition is correct
bool transpositionCorrect(int* matrix_in, int* matrix_out){
    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            if(matrix_in[i*N+j] != matrix_out[j*N+i])
                return false;
    return true;
}


//transposition kernel
__global__ void transpose(int* matrix_in, int* matrix_out){
    int row_idx = blockIdx.x;
    int col_idx = threadIdx.x;
    
    
    while(col_idx < N){ //loop to handle the situation in which N>1024
        matrix_out[N*col_idx + row_idx] = matrix_in[N*row_idx + col_idx];
        col_idx += blockDim.x;
    }
}

int main(int argc, char * argv[]){

    //thread per block is 1024, if not specified otherwise in the argv
    int thread_per_block = MAX_THR_PER_BLOCK;
    if(argc >= 2){
        thread_per_block = atoi(argv[1]);
    }

    fprintf(stdout, "Thread per block: %d\n", thread_per_block);
    
    int * m, * o; //HOST
    int * dev_m, * dev_o; //DEVICE
    int matsize = N * N * sizeof(int);


    //Allocate
    m = (int*)malloc(matsize);
    o = (int*)malloc(matsize);


    cudaMalloc((void**) &dev_m, matsize);
    cudaMalloc((void**) &dev_o, matsize);



    //Initialize
    random_ints(m, N*N);

    //Copy
    cudaMemcpy(dev_m, m, matsize, cudaMemcpyHostToDevice);

    double start = seconds();
    //Run kernel - the triple operator to determine the min between N and 1024
    transpose<<< N , ((N<thread_per_block) ? N : thread_per_block) >>>(dev_m, dev_o);
    cudaDeviceSynchronize();
    double time_exec = seconds()-start;

    // copy device result back to host copy of c
    cudaMemcpy( o, dev_o, matsize,   cudaMemcpyDeviceToHost );
    

    fprintf(stdout, "Orig mx\n");
    sqmatprint(m);

    fprintf(stdout, "Transposed mx\n");
    sqmatprint(o);

    fprintf(stdout, "Execution time: %.6f\n", time_exec);
    fprintf(stdout, "Bandwidth: %.6f\n", matsize * 2 / time_exec / 1e9);

    bool correct = transpositionCorrect(m, o);
    fprintf(stdout, "Transposition is %s\n", correct ? "correct" : "wrong");

    free(m); free(o); cudaFree(dev_m); cudaFree(dev_o);

    return 0;


}