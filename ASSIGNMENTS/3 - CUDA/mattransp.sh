THREADS_PER_BLOCK=(64 512 1024)
REPS=20

module purge
module load cudatoolkit/10.0

rm cudatransp_results.txt

nvcc -o mattransp.out mattransp.cu
nvcc -o fasttransp.out fasttranspose.cu

echo "method time chsize" > cudatransp_results.txt

for thread_per_block in ${THREADS_PER_BLOCK[@]}; do
    for i in $(seq 1 $REPS); do
        mtres=$(./mattransp.out $thread_per_block | grep "Execution time" | sed 's/Execution time: /regular_transp /g')
        echo "$mtres $thread_per_block" >> cudatransp_results.txt
        ftres=$(./fasttransp.out $thread_per_block | grep "Execution time" | sed 's/Execution time: /fast_transp /g')
        echo "$ftres $thread_per_block" >> cudatransp_results.txt
    done
done

rm *.out
module purge