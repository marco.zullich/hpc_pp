# Matrix transposition in CUDA

## Naive matrix transposition

Script name: `mattransp.cu`.

As done at lecture, the kernel `transpose` runs on `N` blocks, where `N` is the side of the matrix, and each block is composed of `N`  threads, up to 1024 threads per block. If the side of the matrix is larger than 1024, there'll be some threads within the block taking care of transposing more than 1 element within the matrix.

The kernel executes an _atomic element transposition_: it copies one element of the original matrix, and pastes it within the transposed inverting the row and column indices.

### Compilation and run

Load module `cudatoolkit`, then compile with `nvcc mattransp.cu`, run with `./a.out [threads_per_block]`.

## Fast matrix transposition

Script name: `fasttranspose.cu`.

This kernel is designed a bit differently from the previous scrpit:

* Now the block and grid design is bidimensional rather than monodimensional. The matrix is subdivided into blocks of 32 * `m` threads, where `m` is a factor dependent upon the `threads_per_block` variable which may be passed as an input parameter to main. Consequently, the 2d grid of blocks is obtained as the matrix side `N` divided by the horizontal size of the block of threads, same for the vertical size.
* The block size is obtained such that, for a maximum of 32*32 threads per block, the submatrix determined by the block fits inside the shared memory of a GPU multiprocessor. A speedup of the transposition is hence obtained by transferring the submatrix from the GPU global to the shared memory, and then transposing the whole submatrix within the global memory once again.

### Compilation and run

Load module `cudatoolkit`, then compile with `nvcc fasttranspose.cu`, run with `./a.out [threads_per_block]`.

## Comparison between the two methods

A comparison may be carried out by submitting a job with the script `mattransp.sh`, which runs the executable for the regular and fast transposition 20 times each, while recording the times of execution for each run.

Submit on Ulysses with `qstat -d "$PWD" -l walltime=3600,nodes=1:ppn=20 -q gpu mattransp.sh`.

The executable will print the times for each run in the same folder, in a file called `cudatransp_results.txt`.

Here is a chart comparing the times of execution (average over 20 runs for each algorithm on 64, 512, 1024 threads per block) on a node of the queue `gpu`:

![CUDA](timecomparison.png "time comparison for CUDA matrix transpose")

The same data in a tabular form:

| Thr. per block | Regular transpose _(ms)_ | Fast transpose _(ms)_ |
| --- | --- | --- |
| 64 | 61.1 | 45.8 |
| 512 | 71.6 | 17.4 |
| 1024 | 66.3 | 18.1 |
