# Approximation of pi via OPENMPI

File `pi_mpi.c`.

The usual approximation of pi is carried out by dividing the [0,1] interval in 1G intervals and assigning them to the processes.

Note: in this exercise, the number of processes (`npes`) has to be a divisor of the number of intervals (`N`), otherwise the program will not run. This because the strategy on remainder handling was yet to be discussed in class, and remainder intervals wouldn't be taken care of, leading to possibly wrong results.

Each process handles statically a chunck of the total intervals, such that process 0 will take care of the intervals from 0 to `N/npes`, process 1 from `N/npes + 1` to `2N/npes` and so on.

A variable `partial_pi` collects the partial approximation of pi for the single process.
When the computation for the single process is completed, the `partial_pi` for the process is reduced into another variabile, `pi_approx`, within the last process of the chain.

To comply with the request, `pi_approx` is then sent to process 0 for the printing via a synchronous Send-Receive, and then the printing is carried out by process 0 only.

## How to compile and run

Load module `openmpi` and compile with `mpicc pi_mpi.c`, run with `mpirun ./a.out -np <NUMBER_OF_PROCESSES>`.
The executable will print the approximation of pi obtained, and the time elapsed.

# Comparison with OPENMP

A slightly modified version of the script written in the first exercise (`ex01_parallel_for_ex03.c`) is present into this folder.
The modification involves mainly the number of intervals (increased from 1e7 to 1e9) so that we compare the data with the openmpi script described above.

Load module `openmpi` and `intel`, compile with `icc ex01_parallel_for_ex03.c -qopenmp`, run with `./a.out -OMP_NUM_THREADS=<NUMBER_OF_THREADS>`.
The executable will print the approximation of pi obtained, and the time elapsed.

A script was written to perform the comparison, `pi_mpi_openmpi_comparison.sh`. It will repeat the run for 4, 8, 16, 32, and 40 threads (processes), 20 times each, and print the time elapsed in a file, `results_comparison.txt`. Averaging out the values for each pair (method, number of threads or processes) will produce the following:

## insert graph here
