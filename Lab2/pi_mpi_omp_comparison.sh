NTHREADS=(4 8 16 32 40)
REPS=5

module purge
module load openmpi


#compile
mpicc -o mpi.out pi_mpi.c

#initialize result file with header
echo "nthreads method time" > results_comparison.txt

#cycle through number of threads(processes) for the specified number of repetitions
for nthread in ${NTHREADS[@]}; do
    for i in $(seq 1 $REPS); do
        #store here time result for mpi single run
        mpi_time=$(mpirun ./mpi.out -np ${nthread} | grep "Elapsed time" | sed 's/Elapsed time: /mpi /g')
        #append to file (with specificacion of nthread)
        echo ${nthread} ${mpi_time} >> results_comparison.txt
    done
done

module load intel
icc -o ompi.out ex01_parallel_for_ex03.c -qopenmp

for nthread in ${NTHREADS[@]}; do
    for i in $(seq 1 $REPS); do
        #store here time result for mpi single run
        ompi_time=$(mpirun ./ompi.out -np ${nthread} | grep "Elapsed time" | sed 's/Elapsed time: /mpi /g')
        #append to file (with specificacion of nthread)
        echo ${nthread} ${mpi_time} >> results_comparison.txt
    done
done

#clean
module purge
rm mpi.out ompi.out