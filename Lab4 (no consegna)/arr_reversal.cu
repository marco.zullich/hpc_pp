#include <stdio.h>
#include <math.h>

#define N 4194304
#define THREADS_PER_BLOCK 512

__global__ void reverse(int* arr_in, int* arr_out){
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    arr_out[N - index] = arr_in[index];
}

void random_ints(int *p, int n) {
	int i;
	for(i=0; i<n; i++) {
		p[i]=rand();
	}
}

int main(void){
    int *arr_in, *arr_out;
    int *dev_in, *dev_out;

    int size = N * sizeof(int);
    
    //alloc host
    arr_in  = (int*)malloc(size);
    arr_out = (int*)malloc(size);
    //alloc device
    cudaMalloc((void**) dev_in , size);
    cudaMalloc((void**) dev_out, size);

    //random initialization in host
    random_ints(arr_in, N);

    //copy to device
    cudaMemcpy(dev_in, arr_in, size, cudaMemcpyHostToDevice);

    //launch kernel
    reverse<<< N / THREADS_PER_BLOCK, THREADS_PER_BLOCK >>> (dev_in, dev_out);
    
    //copy back to host
    cudaMemcpy(arr_out, dev_out, size, cudaMemcpyDeviceToHost);

    //print for check
    FILE *f;
    fopen("arr.out","w");

    fprintf(f,"First 10 elements of array\n");
    int i;
    for(i=0;i<10;i++)
        fprintf(f,"%d, ", arr_in[i]);

    printf("\nLast 10 elements of reversed array\n");
    for(i=N-1;i>N-11;i--)
        fprintf(f,"%d, ", arr_out[i]);
    fclose(f);
    //printf("\n");

    //memory freeing
    free(arr_in); free(arr_out);
    cudaFree(dev_in); cudaFree(dev_out);

    return 0;

    
}

