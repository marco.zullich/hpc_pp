#include <stdio.h>

#define THR_PER_BLK 5
#define N 25

void random_ints(int *p, int n) {
	int i;
	for(i=0; i<n; i++) {
		p[i]=rand();
	}
}

void sqmatprint(int *m){
    int side = sqrt(N);
    for(int i=0; i<N; i++){
        fprintf(stdout, "%d, ", m[i]);
        if((i+1)%side == 0){
            fprintf(stdout, "\n");
        }
    }
}

__global__ void transpose(int* matrix_in, int* matrix_out, int* side){
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    
    int row_idx = index / (*side);
    int col_idx = index % (*side);
    //if not on diagonal
    if(row_idx != col_idx){
        matrix_out[(*side)*col_idx + row_idx] = matrix_in[index];
    }
}

int main(){
    int* m, * o;
    int* dev_m, * dev_o;

    int side = sqrt(N);

    //Allocate
    m = (int*)malloc(N * sizeof(int));
    o = (int*)malloc(N * sizeof(int));
    cudaMalloc((void**)dev_m, N * sizeof(int));
    cudaMalloc((void**)dev_o, N * sizeof(int));

    //Initialize
    random_ints(m, N);

    //Copy
    cudaMemcpy(dev_m, m, N * sizeof(int), cudaMemcpyHostToDevice);

    //Run kernel
    transpose<<<N/THR_PER_BLK, THR_PER_BLK>>>(dev_m, dev_o, &side);

    // copy device result back to host copy of c
    cudaMemcpy( o, dev_o, N * sizeof(int),   cudaMemcpyDeviceToHost );


    fprintf(stdout, "Orig mx\n");
    sqmatprint(m);

    fprintf(stdout, "Transposed mx\n");
    sqmatprint(o);

    free(m); free(o); cudaFree(dev_m); cudaFree(dev_o);

    return 0;


}